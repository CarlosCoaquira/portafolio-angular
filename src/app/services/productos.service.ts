import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../interfaces/producto.interface';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  cargando = true;
  productos: Producto[] = [];
  productosFiltrado: Producto[] = [];

  constructor( private hhtp: HttpClient) {
    this.cargarProducto();

  }

  private cargarProducto(){

    return new Promise( (resolve, reject) => {
      this.hhtp.get('https://angular-html-10b46.firebaseio.com/productos_idx.json')
      .subscribe((resp: Producto[]) => {
        // console.log(resp);

        this.productos = resp;
        this.cargando = false;
        resolve();

      });
    }
    );


  }

  getProducto(id: string){
    return this.hhtp.get(`https://angular-html-10b46.firebaseio.com/productos/${ id }.json`);
  }

  buscarProducto(termino: string){

    if (this.productos.length === 0){
      this.cargarProducto().then( () => {
        this.filtrarProducto(termino);
      } );
    } else {
      this.filtrarProducto(termino);
    }
  }

  private filtrarProducto(termino: string){

    this.productosFiltrado = [];

    termino = termino.toLocaleLowerCase();

    this.productos.forEach( prod => {

      const tituloLowerCase = prod.titulo.toLocaleLowerCase();
      if (prod.categoria.indexOf(termino) >= 0 || tituloLowerCase.indexOf(termino) >= 0){
        this.productosFiltrado.push(prod);
      }
    });

    console.log(this.productosFiltrado);
  }
}
