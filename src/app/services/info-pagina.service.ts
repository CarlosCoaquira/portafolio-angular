import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoPagina } from '../interfaces/info-pagina.interfaces';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  cargada = false;
  info: InfoPagina ;

  equipo: any[] = [];

  constructor(private http: HttpClient) {

    // console.log("Servicio de InfoPagina ejecutado")

    this.cargarInfo();
    this.cargarEquipo();


  }

  private cargarInfo(){
    this.http.get('assets/data/data-pagina.json')
    .subscribe(
        (resp: InfoPagina) => {
        this.cargada = true;
        this.info = resp;
        // console.log(resp);

      // console.log(resp.facebook)
      }
    )
  }

  private cargarEquipo(){
    this.http.get('https://angular-html-10b46.firebaseio.com/equipo.json')
    .subscribe(
        (resp: any[]) => {
        this.equipo = resp;
        // console.log(resp);
      }
    )
}

}
