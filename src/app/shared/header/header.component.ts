import { Component, OnInit, Input } from '@angular/core';
import { InfoPaginaService } from '../../services/info-pagina.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor( public serviceInfo: InfoPaginaService,
               private router: Router ) { }

  // public serviceInfo: InfoPaginaService
  // termino: string,
  ngOnInit() {
  }

  buscarProducto( valor: string) {
    // console.log(termino);
    if(valor.length < 1){
      return;
    }

    this.router.navigate(['/search', valor]);
    // console.log(valor);
  }
 }

